﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SampleApp.Data
{
    public class FileDataRepository : IFileDataRepository
    {
        private readonly string filePath = AppDomain.CurrentDomain.BaseDirectory + "\\SampleAppData.txt";

        public Dictionary<int, string> GetAllRecords()
        {
            try
            {
                string[] records = File.ReadAllLines(filePath);
                Dictionary<int, string> data = new Dictionary<int, string>();
                foreach (string record in records)
                {
                    string[] row = record.Split('\t');
                    data.Add(Convert.ToInt32(row[0]), row[1]);
                }
                return data;
            }
            catch (Exception)
            {
                throw;
            }
           
        }

        public string GetByID(int id)
        {
            Dictionary<int, string> records = GetAllRecords();
            return records.FirstOrDefault(x => x.Key == id).Value;
        }

        public void Insert(string name)
        {
            Dictionary<int, string> records = GetAllRecords();
            int maxID = records.Max(x => x.Key);
            File.AppendAllText(filePath, Environment.NewLine + string.Join("\t", maxID + 1, name));
        }

        public void Update(int id, string name)
        {
            string nameById = GetByID(id);
            var record = File.ReadAllText(filePath);
            record = record.Replace(id.ToString() + "\t" + nameById, id.ToString() + "\t" + name);
            File.WriteAllText(filePath, record);
        }

        public void Delete(int id)
        {
            var tempFile = Path.GetTempFileName();
            var linesToKeep = File.ReadLines(filePath).Where(x => !x.Contains(id.ToString()));

            File.WriteAllLines(tempFile, linesToKeep);

            File.Delete(filePath);
            File.Move(tempFile, filePath);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Data
{
    public interface IFileDataRepository
    {
        Dictionary<int, string> GetAllRecords();
        string GetByID(int id);
        void Insert(string name);
        void Update(int id, string name);
        void Delete(int id);
    }
}

﻿using SampleApp.Data.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Data
{
    public class DataRepository : IDataRepository
    {
        public List<UserInfo> GetUserInfo()
        {
            try
            {
                using (SampleAppEntities db = new SampleAppEntities())
                {
                    return db.UserInfoes.ToList();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        public UserInfo GetByUserInfoId(int id)
        {
            try
            {
                using (SampleAppEntities db = new SampleAppEntities())
                {
                    return db.UserInfoes.FirstOrDefault(x => x.ID == id);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Insert(UserInfo model)
        {
            try
            {
                using (SampleAppEntities db = new SampleAppEntities())
                {
                    db.UserInfoes.Add(model);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Update(UserInfo model)
        {
            try
            {
                using (SampleAppEntities db = new SampleAppEntities())
                {
                    UserInfo userInfo = db.UserInfoes.FirstOrDefault(x => x.ID == model.ID);
                    userInfo.Name = model.Name;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void Delete(int id)
        {
            try
            {
                using (SampleAppEntities db = new SampleAppEntities())
                {
                    UserInfo userInfo = db.UserInfoes.FirstOrDefault(x => x.ID == id);
                    db.UserInfoes.Remove(userInfo);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

﻿using SampleApp.Data.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Data
{
    public interface IDataRepository
    {
        List<UserInfo> GetUserInfo();
        UserInfo GetByUserInfoId(int id);
        void Insert(UserInfo model);
        void Update(UserInfo model);
        void Delete(int id);
    }
}

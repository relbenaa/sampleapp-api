﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SampleApp.Web;
using SampleApp.Web.Controllers;

namespace SampleApp.API.Tests.Controllers
{
    [TestClass]
    public class ValuesControllerTest
    {
        [TestMethod]
        public void Get()
        {
            // Arrange
            FileDataController controller = new FileDataController();

            // Act
            var result = controller.Get();

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(2, result.Count());
            Assert.AreEqual("value1", result.ElementAt(0));
            Assert.AreEqual("value2", result.ElementAt(1));
        }

        [TestMethod]
        public void GetById()
        {
            // Arrange
            FileDataController controller = new FileDataController();

            // Act
            string result = controller.Get(1);

            // Assert
            Assert.AreEqual("Rebina", result);
        }

        [TestMethod]
        public void Post()
        {
            // Arrange
            FileDataController controller = new FileDataController();

            // Act
            controller.Post("value");

            // Assert
        }

        [TestMethod]
        public void Put()
        {
            // Arrange
            FileDataController controller = new FileDataController();

            // Act
            controller.Put(5, "value");

            // Assert
        }

        [TestMethod]
        public void Delete()
        {
            // Arrange
            FileDataController controller = new FileDataController();

            // Act
            controller.Delete(5);

            // Assert
        }
    }
}

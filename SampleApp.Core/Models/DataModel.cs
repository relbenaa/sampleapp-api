﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleApp.Core.Models
{
    public class DataModel
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
    }
}
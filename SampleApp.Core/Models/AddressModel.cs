﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Core.Models
{
    public class AddressModel
    {
        public int ID { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
    }
}

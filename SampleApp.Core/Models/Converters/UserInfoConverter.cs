﻿using SampleApp.Data.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Core.Models.Converters
{
    public class UserInfoConverter
    {
        public List<UserInfoModel> ConvertUserInfoListEntityToModel(List<UserInfo> entity)
        {
            List<UserInfoModel> modelList = new List<UserInfoModel>();
            foreach(UserInfo item in entity)
            {
                UserInfoModel model = new UserInfoModel()
                {
                    ID = item.ID,
                    Name = item.Name
                };
                modelList.Add(model);
            }
            return modelList;
        }

        public UserInfo ConvertModelToEntity(UserInfoModel model)
        {
            return new UserInfo()
            {
                ID = model.ID,
                Name = model.Name
            };
        }

        public UserInfoModel ConvertEntityToModel(UserInfo entity)
        {
            return new UserInfoModel()
            {
                ID = entity.ID,
                Name = entity.Name
            };
        }
    }
}

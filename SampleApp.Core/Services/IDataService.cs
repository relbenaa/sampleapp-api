﻿using SampleApp.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleApp.Core.Services
{
    public interface IDataService
    {
        List<UserInfoModel> GetUserInfo();
        UserInfoModel GetByID(int id);
        void Insert(UserInfoModel model);
        void Update(UserInfoModel model);
        void Delete(int id);
    }
}

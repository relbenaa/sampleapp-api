﻿using SampleApp.Core.Models;
using SampleApp.Core.Models.Converters;
using SampleApp.Data;
using SampleApp.Data.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleApp.Core.Services.Implementation
{
    public class DataService : IDataService
    {
        private IDataRepository dataRepository;
        private UserInfoConverter userInfoConverter;

        public DataService()
        {
            dataRepository = new DataRepository();
            userInfoConverter = new UserInfoConverter();
        }

        public List<UserInfoModel> GetUserInfo()
        {
            List<UserInfo> userInfos = dataRepository.GetUserInfo();
            return userInfoConverter.ConvertUserInfoListEntityToModel(userInfos);
        }

        public UserInfoModel GetByID(int id)
        {
            UserInfo userInfo = dataRepository.GetByUserInfoId(id);
            return userInfo != null ? userInfoConverter.ConvertEntityToModel(userInfo) : new UserInfoModel();
        }

        public void Insert(UserInfoModel model)
        {
            if (model != null)
            {
                UserInfo entity = userInfoConverter.ConvertModelToEntity(model);
                dataRepository.Insert(entity);
            }
        }

        public void Update(UserInfoModel model)
        {
            if (model != null)
            {
                UserInfo entity = userInfoConverter.ConvertModelToEntity(model);
                dataRepository.Update(entity);
            }
        }

        public void Delete(int id)
        {
            dataRepository.Delete(id);
        }
    }
}
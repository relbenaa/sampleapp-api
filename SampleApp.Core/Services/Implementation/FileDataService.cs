﻿using SampleApp.Core;
using SampleApp.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SampleApp.Core.Services.Implementation
{
    public class FileDataService : IFileDataService
    {
        private IFileDataRepository dataRepository;

        public FileDataService()
        {
            dataRepository = new FileDataRepository();
        }

        public Dictionary<int, string> GetAllRecords()
        {
            return dataRepository.GetAllRecords();
        }

        public string GetByID(int id)
        {
            return dataRepository.GetByID(id);
        }

        public void Insert(string name)
        {
            dataRepository.Insert(name);
        }

        public void Update(int id, string name)
        {
            dataRepository.Update(id, name);
        }

        public void Delete(int id)
        {
            dataRepository.Delete(id);
        }
    }
}
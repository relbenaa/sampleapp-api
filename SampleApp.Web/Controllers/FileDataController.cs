﻿using SampleApp.Core;
using SampleApp.Core.Services;
using SampleApp.Core.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SampleApp.Web.Controllers
{
    public class FileDataController : ApiController
    {
        private IFileDataService service;

        public FileDataController()
        {
            service = new FileDataService();
        }
        // GET api/FileData
        public Dictionary<int, string> Get()
        {
            return service.GetAllRecords();
            //return new string[] { "value1", "value2" };
        }

        // GET api/FileData/5
        public string Get(int id)
        {
            return service.GetByID(id);
        }

        // POST api/FileData
        public void Post([FromBody]string value)
        {
            service.Insert(value);
        }

        // PUT api/FileData/5
        public void Put(int id, [FromBody]string value)
        {
            service.Update(id, value);
        }

        // DELETE api/FileData/5
        public void Delete(int id)
        {
            service.Delete(id);
        }
    }
}

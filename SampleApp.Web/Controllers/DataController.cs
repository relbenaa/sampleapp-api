﻿using SampleApp.Core.Models;
using SampleApp.Core.Services;
using SampleApp.Core.Services.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SampleApp.Web.Controllers
{
    public class DataController : ApiController
    {
        private IDataService dataService;

        public DataController()
        {
            dataService = new DataService();
        }

        // GET api/data
        public List<UserInfoModel> Get()
        {
            return dataService.GetUserInfo();
        }

        // GET api/data/5
        public UserInfoModel Get(int id)
        {
            return dataService.GetByID(id);
        }

        // POST api/<controller>
        public void Post([FromBody]UserInfoModel model)
        {
            dataService.Insert(model);
        }

        // PUT api/<controller>/5
        public void Put([FromBody]UserInfoModel model)
        {
            dataService.Update(model);
        }

        // DELETE api/<controller>/5
        public void Delete(int id)
        {
            dataService.Delete(id);
        }
    }
}